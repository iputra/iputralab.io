---
layout: post
title:  "Fail2ban SSH Centos 7"
date:   2020-02-12
tags: centos linux
description: |
  ```bash
  yum install epel-release -y
  yum install fail2ban -y
  systemctl enable fail2ban
  . . .
  ```
---

```bash
yum install epel-release -y
yum install fail2ban -y
systemctl enable fail2ban
nano /etc/fail2ban/jail.local
[DEFAULT]
# Ban hosts for one hour:
bantime = 3600

# Override /etc/fail2ban/jail.d/00-firewalld.conf:
banaction = iptables-multiport

[sshd]
enabled = true

systemctl restart fail2ban
fail2ban-client status
fail2ban-client status sshd
```