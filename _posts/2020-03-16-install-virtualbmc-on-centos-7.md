---
layout: post
title: Install VirtualBMC on Centos 7
tags: centos virtualbmc virtualization
date: 2020-03-16
description: Install virtualBMC on Centos 7 distribution. VirtualBMC is tool for controlling virtual machines using IPMI command . . .
---
Install virtualBMC on Centos 7 distribution. VirtualBMC is tool for controlling virtual machines using IPMI command.

```bash
yum -y update
yum -y install python3-pip python3-devel gcc libvirt-devel ipmitool
pip3 install --upgrade pip
pip3 install virtualbmc

vbmcd
vbmc list

vbmc add ik-metal0 --username admin --password secret@1337 --port 6230
vbmc start ik-metal0

ipmitool -I lanplus -U admin -P secret@1337 -H 10.80.80.1 -p 6230 power status
```
