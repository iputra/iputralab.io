---
layout: post
title:  "Resize Max History on ZSH"
date:   2019-10-26
tags: linux zsh
description: |
  ```bash
  vim .zshrc
  HISTFILE=~/.zsh_history
  HISTSIZE=999999999 
  . . .
  ```
---

```bash
vim .zshrc
HISTFILE=~/.zsh_history
HISTSIZE=999999999
SAVEHIST=$HISTSIZE
```

**References :** 
- [https://unix.stackexchange.com/a/273929](https://unix.stackexchange.com/a/273929)