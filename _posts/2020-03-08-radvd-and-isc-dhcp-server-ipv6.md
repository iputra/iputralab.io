---
layout: post
title: radvd and isc-dhcp-server IPv6
tags: dhcp ipv6 networking radvd
date: 2020-03-08
description: Installation DHCP server for IPv6. Installation does by compiling from source code. DHCP IPv6 need radvd if we don't have router advertisement . . .
---

Installation DHCP server for IPv6. Installation does by compiling from source code. DHCP IPv6 need radvd if we don't have router advertisement.

**Configure Netplan**

```jsx
vi /etc/netplan/50-cloud-init.yaml

network:
    ethernets:
        ens3:
            addresses:
            - 10.40.40.20/24
            gateway4: 10.40.40.1
            nameservers:
                addresses:
                - 8.8.8.8
            accept-ra: no
        ens9:
            addresses:
            - 2001:1200:1100:1000::252/64
            accept-ra: no
            link-local: [ ]
    version: 2
```

**Turn on forwarding for ipv6**

```jsx
sed 's/#net.ipv6.conf.all.forwarding=1/net.ipv6.conf.all.forwarding=1/g' /etc/sysctl.conf | sudo tee /etc/sysctl.conf
echo "net.ipv6.conf.default.forwarding=1" | sudo tee -a /etc/sysctl.conf
sysctl -p
```

**Install and configure radvd**

```jsx
apt install radvd
vi /etc/radvd.conf

interface ens9
{
        AdvSendAdvert on;
        MinRtrAdvInterval 30;
        MaxRtrAdvInterval 100;
        prefix 2001:1200:1100:1000::/64 {
                AdvOnLink on;
                AdvAutonomous on;
                AdvRouterAddr off;
        };

};

systemctl restart radvd
```

**Install DHCP Server**

```jsx
apt update; apt upgrade -y
apt install build-essential git libssl-dev libkrb5-dev

wget https://downloads.isc.org/isc/dhcp/4.4.2/dhcp-4.4.2.tar.gz
tar zxvf dhcp-4.4.2.tar.gz

cd dhcp-4.4.2
mkdir binary
./configure --prefix=${PWD}/binary

sed -i 's/^LIBS =.*/LIBS = -lgssapi_krb5 -lssl -lcrypto -lpthread/g' */Makefile

make
make install

mkdir -p /var/db/
touch /var/db/dhcpd6.leases
```

**Configure dhcpd6.conf**

```jsx
vi dhcpd6.conf

default-lease-time 600;
max-lease-time 7200;
log-facility local7;

subnet6 2001:1200:1100:1000::/64 {
        range6 2001:1200:1100:1000::100 2001:1200:1100:1000::200;
        option dhcp6.name-servers 2001:4086:4086::8888;
        option dhcp6.domain-search "ns.putra.io";
}
```

**Run DHCPD Server**

```jsx
./server/dhcpd -6 -d -cf dhcpd6.conf ens9
```

**Running On Client**

```jsx
# Copy dhclient from dhcp-server node
# run this from dhcp-server node
scp dhcp-4.4.2/client/dhclient ubuntu@ik-client1:

# On Client node
/home/ubuntu/dhclient -6 ens9
```

**Ref :** 

- [https://blog.netpro.be/dhcpv6-configuration-isc-dhcp-server/](https://blog.netpro.be/dhcpv6-configuration-isc-dhcp-server/)
- [http://isc-dhcp-users.2343191.n4.nabble.com/Building-DHCP-server-from-sources-repository-td2352.html](http://isc-dhcp-users.2343191.n4.nabble.com/Building-DHCP-server-from-sources-repository-td2352.html)
- [http://www.linuxfromscratch.org/blfs/view/8.3/basicnet/dhcp.html](http://www.linuxfromscratch.org/blfs/view/8.3/basicnet/dhcp.html)
- [https://www.youtube.com/watch?v=958sRC306UE](https://www.youtube.com/watch?v=958sRC306UE)
- [https://mirrors.deepspace6.net/Linux+IPv6-HOWTO/x1159.html](https://mirrors.deepspace6.net/Linux+IPv6-HOWTO/x1159.html)
- [https://wiki.ubuntu.com/JonathanFerguson/radvd](https://wiki.ubuntu.com/JonathanFerguson/radvd)
- [https://www.jumpingbean.co.za/blogs/mark/set-up-ipv6-lan-with-linux](https://www.jumpingbean.co.za/blogs/mark/set-up-ipv6-lan-with-linux)
- [https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/networking_guide/sec-configuring_the_radvd_daemon_for_ipv6_routers](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/networking_guide/sec-configuring_the_radvd_daemon_for_ipv6_routers)
- [https://tldp.org/HOWTO/Linux+IPv6-HOWTO/ch22s04.html](https://tldp.org/HOWTO/Linux+IPv6-HOWTO/ch22s04.html)
